package fr.cnam.foad.nfa035.badges.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

/**
 * Gérer les services Rest avec des entrées mixtes
 */
@Component
public class MultipartJacksonToHttpMessage extends AbstractJacksonToHttpMessage {

    /**
     *Convertisseur pour supporter les requêtes HTTP
     * @param objectMapper
     */
    public MultipartJacksonToHttpMessage(ObjectMapper objectMapper) {
        super(objectMapper, MediaType.APPLICATION_OCTET_STREAM);
    }

    /**
     * {@inheritDoc}
     * @param clazz
     * @param mediaType
     * @return
     */
    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    /**
     * {@inheritDoc}
     * @param type
     * @param clazz
     * @param mediaType
     * @return
     */
    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        return false;
    }

    /**
     * {@inheritDoc}
     * @param mediaType
     * @return
     */
    @Override
    protected boolean canWrite(MediaType mediaType) {
        return false;
    }
}





