package fr.cnam.foad.nfa035.badges.wallet.model;

public class MD5HashValueFilter {

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return true;
        String value = (String) other;
        return value.equals("<n/a>");
    }

}
